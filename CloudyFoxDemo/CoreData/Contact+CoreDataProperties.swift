//
//  Contact+CoreDataProperties.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var userId: Int32
    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var isComplete: Bool

}

extension Contact : Identifiable {

}
