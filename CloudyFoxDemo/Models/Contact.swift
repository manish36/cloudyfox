//
//  Contact.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import Foundation
struct ContactApi: Decodable {
  let userId: Int?
  let id: Int?
  let title: String?
  let isComplete: Bool?
  
  enum CodingKeys: String, CodingKey {
    case userId
    case id
    case title
    case isComplete = "completed"
  }
}
