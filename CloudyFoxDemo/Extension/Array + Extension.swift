//
//  Array + Extension.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import Foundation
import Foundation
extension Array {
    
    func element(at index: Int) -> Element? {
        if index >= .zero && index < count {
            return self[index]
        }
        return nil
    }
    
}
