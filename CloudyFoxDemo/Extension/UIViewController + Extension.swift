//
//  UIViewController + Extension.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import UIKit
extension UIViewController {
    //MARK: Alert
    func getAlert(message: String?, title: String?, style: UIAlertController.Style? = .alert) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    func alert(message: String?) {
        let alertController = getAlert(message: message, title: nil)
        alertController.addAction(title: GlobalConstant.shared.OK, handler: nil)
        alertController.modalPresentationStyle = .fullScreen
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alert(message: String?, title: String? = "",  okAction: (()->())? = nil) {
        let alertController = getAlert(message: message, title: title)
        alertController.addAction(title: GlobalConstant.shared.OK, handler: okAction)
        alertController.modalPresentationStyle = .fullScreen
        self.present(alertController, animated: true, completion: nil)
    }

}

//MARK: UIAlertController
extension UIAlertController {
    
    func addAction(title: String?, style: UIAlertAction.Style = .default, handler: (()->())? = nil) {
        let action = UIAlertAction(title: title, style: style, handler: {_ in
            handler?()
        })
        self.addAction(action)
    }
    
}
