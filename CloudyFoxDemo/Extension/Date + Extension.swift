//
//  Date + Extension.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import Foundation
extension Date {
    func toString(format: String = "yyyy/MM/dd HH:mm") -> String {
          let dateFormatter = DateFormatter()
          dateFormatter.locale = Locale.init(identifier: "en_US")
          dateFormatter.dateStyle = .long
          dateFormatter.timeStyle = .short
          return dateFormatter.string(from: self)
      }
}
