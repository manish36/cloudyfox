//
//  GlobalConstant.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import Foundation
struct GlobalConstant {
    
    static let shared = GlobalConstant()
    
    let OK = "Ok"
    let Opps = "!Oops"
}
