//
//  CustomFont.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import UIKit

enum CustomFont: String {
    
    case robotoBold = "Roboto-Bold"
    case robotoRegular = "Roboto-Regular"
    case robotoMedium = "Roboto-Medium"
    case robotoItalic = "Roboto-Italic"
    case dynamic
    
    func of(_ heading: Heading) -> UIFont? {
        return UIFont(name: self.rawValue, size: heading.size)
    }
    
}

enum Heading {
    
    case heading1
    case heading2
    case heading3
    case heading4
    case customHeading(size: CGFloat)
    
    var size: CGFloat {
        switch self {
        case .heading1:
            return 16
        case .heading2:
            return 14
        case .heading3:
            return 12
        case .heading4:
            return 10
        case .customHeading(let size):
            return size
        }
    }
    
}
