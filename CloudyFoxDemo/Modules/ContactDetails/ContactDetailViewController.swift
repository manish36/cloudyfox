//
//  ContactDetail.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import UIKit

class ContactDetailViewController: UIViewController {
    
    //MARK: PROPERTIES
    let deeplink = "contactdemo://id="
    let copied = "Copied"
    weak var coordinator: BaseCoordinator?
    var contact: Contact?
    
        
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var shareButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        configure()
    }
    
    //MARK: IBACTION
    @IBAction func didTapLinkCopy(_ sender: Any) {
        
        if let id = contact?.id {
        let url = deeplink + "\(id)"
            UIPasteboard.general.string = url
            alert(message: url, title: copied, okAction: nil)
        } else {
            alert(message: GlobalConstant.shared.Opps)
        }
    }
    
    private func setup() {
        uiSetup()
    }
    
    private func uiSetup() {
        titleLabel?.textColor = .gray
        titleLabel?.font = CustomFont.robotoItalic.of(.heading2)
    
        shareButton.setTitle("Copy", for: .normal)
        shareButton.setTitleColor(UIColor.systemBlue, for: .normal)
    }
    
    private func configure() {
        titleLabel?.text = contact?.title
    }
}

extension ContactDetailViewController: Coordinating {
    
}
