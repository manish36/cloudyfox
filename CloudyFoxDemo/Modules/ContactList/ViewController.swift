//
//  ViewController.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 23/08/2021.
//

import UIKit
import CoreData
class ViewController: UIViewController {
    
    //MARK: Properties
    var context:  NSManagedObjectContext?
    weak var coordinator: BaseCoordinator?
    private var contact: [Contact]? {
        didSet {
            if let _ = contact {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    //MARK: OTHER ACTION
    private func setup() {
        uiSetup()
        fetchContact()
        setupTableView()
    }
    
    private func uiSetup() {
        view.backgroundColor = .gray
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: ContactDetailsTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ContactDetailsTableViewCell.identifier)
        tableView.separatorStyle = .none
    }
    
    private func fetchContact() {
        coordinator?.fetchContact { [weak self] result in
            self?.contact = result
        } failure: { [weak self] error in
            print(error)
        }
    }
}

//MARK: TABLEVIEW EXTENSION
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contact?.count ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactDetailsTableViewCell.identifier, for: indexPath) as! ContactDetailsTableViewCell
        cell.contact = contact?.element(at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20.0
    }
    
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let  id = contact?.element(at: indexPath.row)?.id {
            coordinator?.presentDetails(id: Int(id))
        }
    }
}

//MARK: Coodinator extension
extension ViewController: Coordinating {
    
}

