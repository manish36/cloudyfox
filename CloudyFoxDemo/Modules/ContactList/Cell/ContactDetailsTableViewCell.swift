//
//  ContactDetailsTableViewCell.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 24/08/2021.
//

import UIKit

class ContactDetailsTableViewCell: UITableViewCell {
    
    //Constant
    static var identifier: String {
            return String(describing: self)
    }
    
    //MARK: Properties
    var contact: Contact? {
        didSet {
                configure(contact)
        }
    }
    
    var date = Date().toString()
    
    //MARK: IBOUTLETS
    @IBOutlet weak var userInfoLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        commonSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        commonSetup()
    }
    
    private func commonSetup() {
        userInfoLabel?.font = CustomFont.robotoBold.of(.heading3)
        userInfoLabel?.textColor = UIColor(hex: "#BB595F")
        dateLabel?.font = CustomFont.robotoRegular.of(.heading4)
        dateLabel?.textColor = UIColor(hex: "#F8DEE7")
    }
    
    private func configure(_ contact: Contact?) {
        userInfoLabel?.text = contact?.title
        dateLabel?.text = date
    }
    
}
