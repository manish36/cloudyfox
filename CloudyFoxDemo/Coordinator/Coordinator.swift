//
//  Coordinator.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 23/08/2021.
//

import Foundation
import UIKit
import CoreData

protocol Coordinator {
    var childCoordinator: [Coordinator] {get set}
    var navigationContrtoller: UINavigationController? { get set }
    var context:  NSManagedObjectContext? { get set }
    
    func start()
    func fetchDataFromApi(success: @escaping ([ContactApi]) -> (), failure: @escaping (Error) -> ())
}
