//
//  Coordinating.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 23/08/2021.
//

import Foundation
import UIKit
protocol Coordinating {
    static func instantiate(_ storyboard: String) -> Self
}

extension Coordinating where Self: UIViewController {
    static func instantiate(_ storyboard: String) -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: storyboard, bundle: Bundle.main)
        return storyboard.instantiateViewController(identifier: id) as! Self
    }
}
