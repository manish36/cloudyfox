//
//  BaseCoordinator.swift
//  CloudyFoxDemo
//
//  Created by Manish Adhikari on 23/08/2021.
//

import Foundation
import UIKit
import CoreData
class BaseCoordinator: Coordinator {
  
    
    
    var childCoordinator = [Coordinator]()
    var navigationContrtoller: UINavigationController?
    var context:  NSManagedObjectContext? = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    

    
     init(navigationController: UINavigationController?) {
        self.navigationContrtoller = navigationController
    }
    
    //MARK: NAVIGATION ROUTING
    func start() {
        let vc = ViewController.instantiate("Main")
        vc.coordinator = self
        vc.context = context
        navigationContrtoller?.pushViewController(vc, animated: false)
        
    }
    
    func presentDetails(id: Int) {
       
        let vc = ContactDetailViewController.instantiate("Main")
        vc.coordinator = self
        vc.contact =  fetchSingleContext(id: id)
        navigationContrtoller?.pushViewController(vc, animated: true)
    }
    
    //MARK: API HANDLER
    
    func fetchDataFromApi(success: @escaping ([ContactApi]) -> (), failure: @escaping (Error) -> ()) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/todos")!
          URLSession.shared.fetchContact(for: url) { (result: Result<[ContactApi], Error>) in
            switch result {
            case .success(let toDos):
              success(toDos)
            case .failure(let error):
                failure(error)
          }
        }
    }
    
    func fetchContact(success: @escaping ([Contact]?) -> (), failure: @escaping (Error) -> ()) {
        fetchDataFromApi { [weak self] result in
            success(self?.saveContextAndFetchContext(result))
        } failure: { [weak self] error in
            failure(error)
        }
    }
    
    //MARK: LOCAL STORAGE
    func saveContextAndFetchContext(_ data: [ContactApi]) -> [Contact]? {
        
        var contact: [Contact]?
        if let context = context {
            
            //More than 100 data is fetch from api
            for i in 1 ... 10  {
                let contact = Contact(context: context)
                
                guard let id = data.element(at: i)?.id else { continue }
                guard let userId = data.element(at: i)?.userId else { continue }
                contact.id = Int32(id)
                contact.userId = Int32(userId)
                contact.title = data[i].title
                contact.isComplete = data[i].isComplete ?? false
            }
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            do {
                contact = try context.fetch(Contact.fetchRequest())
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        }
        return contact
    }
    
    func fetchSingleContext(id: Int) -> Contact? {
        var contact: Contact?
        if let context = context {
           let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
           let predicate = NSPredicate(format: "id = %@", argumentArray: [id])
           fetch.predicate = predicate

           do {
            contact = try context.fetch(fetch).first as? Contact
           } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
           }
        }
        
        return contact
    }

}
