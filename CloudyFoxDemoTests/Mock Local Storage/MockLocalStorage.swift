//
//  MockLocalStorage.swift
//  CloudyFoxDemoTests
//
//  Created by Manish Adhikari on 24/08/2021.
//

import Foundation
import CoreData

class MockLocalStorage: NSObject {
    lazy var persistentContainer: NSPersistentContainer = {
        let description = NSPersistentStoreDescription()
        description.url = URL(fileURLWithPath: "/dev/null")
        let container = NSPersistentContainer(name: "Contact")
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()
}
