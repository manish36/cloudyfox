//
//  CloudyFoxDemoTests.swift
//  CloudyFoxDemoTests
//
//  Created by Manish Adhikari on 24/08/2021.
//

import XCTest
import CoreData
@testable import CloudyFoxDemo

class CloudyFoxDemoTests: XCTestCase {
    
    //MARK: TypeAlias
    typealias MockedContactApi = ContactApi
    typealias MockedContact = Contact
    
    //MARK: Properties
    var mockedPersistentContainer: NSPersistentContainer?
    var mockedCoordinator: BaseCoordinator?
    var mockedContactApi: [MockedContactApi]!
    
    override func setUpWithError() throws {
        mockedPersistentContainer = MockLocalStorage().persistentContainer
        mockedCoordinator?.context = mockedPersistentContainer?.newBackgroundContext()
        mockedCoordinator = BaseCoordinator(navigationController: nil)
        
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        mockedPersistentContainer = nil
        mockedCoordinator?.context = nil
        mockedPersistentContainer = nil
    }
    
    /**
     Title: save mocked data and fetch
     Description: Moceked contactApi models are saved,and fetched
     Exceptation: fetched data should be eqaul to given mocked data
     */
    func testSaveAndFetchDataCaseHappy() throws {
        mockedContactApi = [
            MockedContactApi(userId: 1, id: 1, title: "test1", isComplete: false),
            MockedContactApi(userId: 2, id: 2, title: "test2", isComplete: true),
            MockedContactApi(userId: 3, id: 3, title: "test3", isComplete: true)
        ]
        
        var fetchedMockedData = mockedCoordinator?.saveContextAndFetchContext(mockedContactApi)

        let doesContain = fetchedMockedData!.contains { $0.title == "test1" }// test1 is assigned
        XCTAssertTrue(doesContain)
    }
    
    /**
     Title: save mocked data and fetch
     Description: Moceked contactApi models are saved,and fetched
     Exceptation: fetched data should  not  eqaul to given mocked data
     */
    func testSaveAndFetchDataCaseRandomUserIdCheck() throws {
        mockedContactApi = [
            MockedContactApi(userId: 1, id: 1, title: "test1", isComplete: false),
            MockedContactApi(userId: 2, id: 2, title: "test2", isComplete: true),
            MockedContactApi(userId: 3, id: 3, title: "test3", isComplete: true)
        ]
        
        var fetchedMockedData = mockedCoordinator?.saveContextAndFetchContext(mockedContactApi)?.first
        let doesContain = mockedContactApi.contains { $0.title == "Hello123" } // Userid 1 is assigned
        XCTAssertFalse(doesContain)
    }
    
    /**
     Title: save mocked data and fetch signle particular data
     Description: Moceked contactApi models are saved,and fetched particular data
     Exceptation: fetched data should   eqaul to given mocked data
     */
    func testFetchParticularDataCaseHappyCase() throws {
        mockedContactApi = [
            MockedContactApi(userId: 1, id: 1, title: "test1", isComplete: false),
            MockedContactApi(userId: 2, id: 2, title: "test2", isComplete: true),
            MockedContactApi(userId: 3, id: 3, title: "test3", isComplete: true)
        ]
        
        _ = mockedCoordinator?.saveContextAndFetchContext(mockedContactApi)?.first
        
        let contact = mockedCoordinator?.fetchSingleContext(id: 2)
        XCTAssertEqual(contact?.id, 2)
    }
    
    /**
     Title: save mocked data and fetch signle particular data of not existed id
     Description: Moceked contactApi models are saved,and fetched particular data
     Exceptation: fetched data should   nil
     */
    func testFetchParticularDataCaseNotExistedId() throws {
        mockedContactApi = [
            MockedContactApi(userId: 1, id: 1, title: "test1", isComplete: false),
            MockedContactApi(userId: 2, id: 2, title: "test2", isComplete: true),
            MockedContactApi(userId: 3, id: 3, title: "test3", isComplete: true)
        ]
        
        _ = mockedCoordinator?.saveContextAndFetchContext(mockedContactApi)?.first
        
        let contact = mockedCoordinator?.fetchSingleContext(id: 5)
       XCTAssertNil(contact)
    }



}
